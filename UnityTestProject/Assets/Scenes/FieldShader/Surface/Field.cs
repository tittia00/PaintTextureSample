﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Field : MonoBehaviour
{
    private Material material;
    // Start is called before the first frame update
    void Start()
    {
        material = GetComponent<Renderer>().material;

        _Pos[0] = new Vector4( 0.5f , 0, 0, 0);
        _Pos[1] = new Vector4( 0.25f, 0, 0, 0);
        _Pos[2] = new Vector4(-0.5f , 0, 0, 0);
        _Pos[3] = new Vector4(-0.25f, 0, 0, 0);
        _Pos[4] = new Vector4(     0, 0, 0, 0);



        _Pos[0] = new Vector4(   0f, -3.0f, 0, 0);
        _Pos[1] = new Vector4( 1.0f, -2.0f, 0, 0);
        _Pos[2] = new Vector4(-2.0f, 1.0f, 0, 0);
        _Pos[3] = new Vector4( 0.0f, 0.0f, 0, 0);


        _Pos[0] = new Vector4(0.0f, 0.0f, 0, 0);
        _Pos[1] = new Vector4(0.0f, 0.0f, 0, 0);
        _Pos[2] = new Vector4(0.0f, 0.0f, 0, 0);
        _Pos[3] = new Vector4(0.0f, 0.0f, 0, 0);
        _Pos[4] = new Vector4(2.25f, 0f, 0, 0);
        _Pos[5] = new Vector4(-2.25f, -2.25f, 0, 0);
        _Pos[6] = new Vector4( 2.25f, -2.25f, 0, 0);
        _Pos[7] = new Vector4( 2.25f,  2.25f, 0, 0);
        _Pos[8] = new Vector4(-2.25f,  2.25f, 0, 0);


        //        -4.5
        //        +4.5
    }

    public Vector4[] _Pos = new Vector4[9];

    // Update is called once per frame
    void Update()
    {
        material.SetVectorArray("_Pos", _Pos);
    }
}
