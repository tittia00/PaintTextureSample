﻿Shader "Custom/FieldSurfaceShader" {
	Properties
	{
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_StampTex("Albedo (RGB)", 2D) = "white" {}
	}
	SubShader{
		Tags { "Queue" = "Transparent" }
		LOD 200

		CGPROGRAM
		#pragma surface surf Standard alpha:fade 

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _StampTex;
		float2 _Pos[9];

		struct Input
		{
			float3 worldPos;
			float2 uv_MainTex;
			float2 uv_StampTex;
		};




		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
		// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)



		void surf(Input IN, inout SurfaceOutputStandard o) {

			fixed4 c = tex2D(_MainTex, IN.uv_MainTex * 1);

			float smooth = 0.5;
			float metallic = 0.5;

			float2 uv;

			for (int i = 0;i<9;i++) {

				uv = IN.uv_StampTex;
				uv.x += _Pos[i].x / 4.5;
				uv.y += _Pos[i].y / 4.5;

				fixed4 mask = tex2D(_StampTex, uv);
				
				if (mask.a > 0)
				{
					c = mask;
					//c = float4(0,1,0,1);
					//o.Smoothness = smooth;
					//o.Metallic = metallic;
				}
			}

			o.Alpha = 1;


			float dist = distance(fixed3(0, 0, 0), IN.worldPos);
			float radius = 2;
			if (dist < radius) {
				o.Albedo = c ;
			}
			else {
				o.Albedo = c;
			}
		}
		ENDCG
	}
	FallBack "Diffuse"
}
