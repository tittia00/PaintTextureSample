﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Es.TexturePaint.Sample
{
    public class MousePainter : MonoBehaviour
    {
        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hitInfo;
                if (Physics.Raycast(ray, out hitInfo))
                {
                    //var paintObject = hitInfo.transform.GetComponent<DynamicPaintObject2>();
                    //if (paintObject != null)
                    //    paintObject.Paint(hitInfo);




                    var material = hitInfo.transform.gameObject.GetComponent<Renderer>().material;

                    

                    Vector4[] _Pos = new Vector4[2];

                    Debug.Log("uv:"+hitInfo.textureCoord);

                    _Pos[0] = new Vector4(hitInfo.textureCoord.x, hitInfo.textureCoord.y, 0, 0);
                    //_Pos[1] = new Vector4(0.0f, 0.0f, 0, 0);




                    material.SetVectorArray("_Pos", _Pos);
                }
            }
        }
    }
}
